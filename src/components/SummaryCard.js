import React from 'react'
import {Card, Progress} from "antd";

const {Meta} = Card;
const SummaryCard = () => {

    return (
        <Card>
            <Meta
                avatar={<Progress width={40} type="circle" percent={75}/>}
                title="Card title"
                description="This is the description"
            />
        </Card>
    )
}
export default SummaryCard;
