import React from 'react'
import {Input, message} from "antd";
import  axios from 'axios';


const SearchField = ({  setTestCases,setProjectName }) => {
    console.log(" SearchField Components   Re render")
    const onSearch = async value => {
        try  {
            // this api return all test case of projects name
            const res =  await axios.get(`/api/log/${value}/all`)
            console.log(res)
            setProjectName(value);
            setTestCases(res.data);
            message.success("Choose testcase below")
        }catch(err) {
            message.error("Wrong Value Of Project Name")
            setTestCases([])
        }
    }
    return <Input.Search  placeholder ="search token for project"  onSearch = {onSearch} />
}

export  default  SearchField;
