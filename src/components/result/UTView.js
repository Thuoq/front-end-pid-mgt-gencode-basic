import {Table} from "antd";
import React from "react";

export default function UTView({classes}) {

    const columns = [
        {
            title: "Name",
            dataIndex: ['name'],
            key: "name",
        },
        {
            title: "LOC",
            dataIndex: "loc",
            key: "lines",
        },
        {
            title: "Functions and Methods",
            dataIndex: ["methods", 'length'],
            key: "Functions",
            render: (value, row, index) => {
                // do something like adding commas to the value or prefix
                if (value) {
                    return <span>{value}</span>;
                } else {
                    return <span>n/a</span>
                }
            }
        },
        {
            title: "Source LOC",
            dataIndex: "sloc",
            key: "Sloc",
        },
        {
            title: "Coverage",
            dataIndex: "coverage",
            key: "Sloc",
            render: (value, row, index) => {
                // do something like adding commas to the value or prefix
                if (value) {
                    return <span>{value}%</span>;
                }
            }
        }
    ];

    return (
        <Table
            size="small"
            columns={columns}
            expandable={{
                rowExpandable: record => record.children.length === 0,
            }}
            dataSource={classes}
        />
    );
}
