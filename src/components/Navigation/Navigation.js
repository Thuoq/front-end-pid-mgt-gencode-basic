import React from 'react'
import {Layout, Menu} from 'antd';
import "./style.css"

const {Header} = Layout;

const Navigation = () => {
    return (
        <Header
            style={{
                zIndex: 1,
                width: '100%',
            }}
        >
            <div className="logo"/>
            <Menu
                theme="dark"
                style={{
                    display: "flex",
                    justifyContent: "flex-end"
                }}
                mode="horizontal"
                defaultSelectedKeys={['1']}
                items={['Home', 'Sign In'].map((_, index) => ({
                    key: String(index + 1),
                    label: `${_}`,
                }))}
            />
        </Header>
    )
}

export default Navigation;
