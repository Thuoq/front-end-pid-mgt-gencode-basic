import React from 'react';
import { Select , message} from 'antd';
import axios from "axios";

const { Option } = Select;

const DropDownSelect = ({projectName, setClasses, testCases}) => {
    console.log(" DropDownSelect Components   Re render")
    const onSelect =  async  (value) => {
        try {
            const resClasses = await axios.get(`/api/log/${projectName}/${value}/class`)
            setClasses(resClasses.data)
            message.success("See Result Each Test Case Below")
        }catch (error) {
            message.error("Invalid name TestCase")
            setClasses([])
        }



    }
    return (
        <Select
            showSearch
            style={{
                width: `100%`,
            }}
            onSelect={onSelect}
            placeholder="Search to Log Name "
            optionFilterProp="children"
            filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
            filterSort={(optionA, optionB) =>
                optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
            }
        >
            {
                testCases.map((el,idx) => (
                    <Option key={idx} value={el}>{el}</Option>
                ))
            }
        </Select>
    )
}




export default DropDownSelect;
