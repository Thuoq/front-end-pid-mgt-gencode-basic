import UTView from './components/result/UTView';
import SearchField from "./components/SearchField";
import DropDownSelect from "./components/DropDownSelect";
import Navigation from './components/Navigation/Navigation'
import {Fragment, useState} from "react";
import {Layout} from "antd";
import "./App.css"

const {Content} = Layout;

function App() {
    const [projectName, setProjectName] = useState("")
    const [tesCases, setTestCases] = useState([])
    const [classes, setClasses] = useState([])
    return (
        <Layout>
            <Navigation/>
            <Content
                className="site-layout"
                style={{
                    padding: '0 50px',
                    marginTop: 64,
                }}
            >
                <div
                    className="site-layout-background"
                    style={{
                        padding: 24,
                        minHeight: "100vh",
                    }}
                >
                    <SearchField setProjectName={setProjectName} setTestCases={setTestCases}/>
                    {
                        tesCases.length ? (
                            <Fragment>
                                <DropDownSelect projectName={projectName} setClasses={setClasses} testCases={tesCases}/>
                                {/*<SummaryCard/>*/}
                                {
                                    classes.length ? <UTView classes={classes}/> : null
                                }
                            </Fragment>
                        ) : null
                    }
                </div>
            </Content>
        </Layout>
    );
}

export default App;
